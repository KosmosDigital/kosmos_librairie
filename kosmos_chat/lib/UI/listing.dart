import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChatListing extends StatefulWidget {
  final List<dynamic> nodes;
  final String nodeImageKey;
  final String nodePseudoKey;
  final String nodeMessageKey;

  ChatListing({
    required this.nodes,
    required this.nodeImageKey,
    required this.nodePseudoKey,
    required this.nodeMessageKey,
  });

  @override
  _ChatListingState createState() => _ChatListingState();
}

class _ChatListingState extends State<ChatListing> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(454.7368421052632, 985.2631578947369),
      builder: () {
        return Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: new SingleChildScrollView(
            child: new Column(
              children: [
                new SizedBox(height: MediaQuery.of(context).padding.top + 30.h),
                
              ],
            ),
          ),
        );
      }
    );
  }
}